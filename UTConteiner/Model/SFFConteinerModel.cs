﻿using System;
using TechTalk.SpecFlow.Assist.Attributes;

namespace UTConteiner.Model
{
  public  class SFFConteinerModel
    {

        [TableAliases("Transportadora")]
        public string Transportadora { get; set; }
        [TableAliases("Booking")]
        public string Booking { get; set; }
        [TableAliases("Conteiner")]
        public string Conteiner { get; set; }
        [TableAliases("Lacre")]
        public string Lacre { get; set; }
        [TableAliases("PesoBruto")]
        public string PesoBruto { get; set; }
        [TableAliases("Numero")]
        public string Numero { get; set; }
        [TableAliases("Danfe")]
        public string Danfe { get; set; }


    }
}
