﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using UTConteiner.Model;

namespace UTConteiner.CodeBinding
{
    [Binding]
    public class SFTESTEStepsSteps
    {
        IWebDriver WDriver;
        SFFConteinerModel sFFConteinerModel = new SFFConteinerModel();

        private bool IsElementPresent(By by)
        {
            try
            {
                WDriver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        [Given(@"entrar com url")]
        public void GivenEntrarComUrl()
        {
            WDriver = new ChromeDriver(@"C:\driver\");
            WDriver.Navigate().GoToUrl("https://portaldeservicos-dev-modal.grupolibra.com.br");
        }

        [Given(@"entrar com usuario e senha")]
        public void GivenEntrarComUsuarioESenha()
        {
            WDriver.FindElement(By.Id("_58_login")).SendKeys("alexandre.ferreira@modalgr.com.br");
            WDriver.FindElement(By.Id("_58_password")).SendKeys("modal123456");
            WDriver.FindElement(By.ClassName("btn-primary")).Click();
        }

        [Given(@"entrar com transportadora (.*)")]
        public void GivenEntrarComTransportadora(string lTransportadora)
        {
            sFFConteinerModel.Transportadora = lTransportadora;
            WDriver.FindElement(By.Id("showMenu")).Click();
            WDriver.FindElement(By.Id("layout_30")).Click();

            WebDriverWait wait = new WebDriverWait(WDriver, TimeSpan.FromSeconds(10));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.Id("txtEntidade")));

            WDriver.FindElement(By.Id("txtEntidade")).SendKeys(sFFConteinerModel.Transportadora);
        }

        [Given(@"entrar com booking (.*)")]
        public void GivenEntrarComBooking(string lBooking)
        {
            sFFConteinerModel.Booking = lBooking;

            WDriver.FindElement(By.XPath("//*[@id='portlet_ConsultaBooking_WAR_ExportacaoCheioVazioPortlet']/div/div/div/div/div[3]/div[4]/div/div[1]/input")).SendKeys(sFFConteinerModel.Booking);
            WDriver.FindElement(By.XPath("//*[@id='portlet_ConsultaBooking_WAR_ExportacaoCheioVazioPortlet']/div/div/div/div/div[3]/div[4]/div/div[2]/a[1]")).Click();
        }

        [Given(@"Entrar com Unidade (.*)")]
        public void GivenEntrarComUnidade(int lUnidade)
        {
            WebDriverWait wait = new WebDriverWait(WDriver, TimeSpan.FromSeconds(10));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='portlet_ConsultaBooking_WAR_ExportacaoCheioVazioPortlet']/div/div/div/div/div[3]/div[9]/div/ul/li[1]/a")));
            lUnidade += 1;
            WDriver.FindElement(By.XPath("//*[@id='grid']/div/table/tbody/tr/td[2]")).Click();

            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='grid']/div/table/tbody/tr[" + lUnidade + "]/td[2]/a")));

            WDriver.FindElement(By.XPath("//*[@id='grid']/div/table/tbody/tr[" + lUnidade + "]/td[2]/a")).Click();

        }
        [Given(@"editar unidade")]
        public void GivenEditarUnidade()
        {
            WebDriverWait wait = new WebDriverWait(WDriver, TimeSpan.FromSeconds(10));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.Id("grid")));

            

            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='portlet_ConsultaBooking_WAR_ExportacaoCheioVazioPortlet']/div/div/div/div/div[3]/div[9]/div/ul/li[1]/a")));

            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.LinkText("Reserva de Janela")));
        }

        [Given(@"entrar com o conteiner (.*)")]
        public void GivenEntrarComNumeroDoConteiner(string lConteiner)
        {

            sFFConteinerModel.Conteiner = lConteiner;
           
            var wait = new WebDriverWait(WDriver, TimeSpan.FromSeconds(10));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='portlet_ConsultaBooking_WAR_ExportacaoCheioVazioPortlet']/div/div/div/div/div[3]/div[9]/div/ul/li[1]/a")));

            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.Id("numeroContainer")));

            WDriver.FindElement(By.Id("numeroContainer")).SendKeys(sFFConteinerModel.Conteiner);
        }

        [Given(@"confirmar conteiner")]
        public void GivenConfirmarConteiner()
        {
            WDriver.FindElement(By.XPath("//*[@id='container']/div[2]/div[1]/form/div[4]/div/a[1]")).Click();
            WebDriverWait wait = new WebDriverWait(WDriver, TimeSpan.FromSeconds(30));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.Id("divConfirmSave")));

            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id='divConfirmSave']/p[2]/a[1]")));

            WDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div/div/section/div/div/div/div/div[3]/div[10]/div[4]/div/div[2]/div[4]/div/p[2]/a[1]/i")));
            WDriver.FindElement(By.XPath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div/div/section/div/div/div/div/div[3]/div[10]/div[4]/div/div[2]/div[4]/div/p[2]/a[1]/i")).Click();

            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(By.XPath("//*[@id='portlet_ConsultaBooking_WAR_ExportacaoCheioVazioPortlet']/div/div/div/div/div[3]/div[1]/div[3]/div")));
            WDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            WebDriverWait wait30 = new WebDriverWait(WDriver, TimeSpan.FromSeconds(30));
            element = wait30.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div/div/section/div/div/div/div/div[3]/div[1]/div[3]/div/p[2]/a/i")));

            WDriver.FindElement(By.XPath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div/div/section/div/div/div/div/div[3]/div[1]/div[3]/div/p[2]/a/i")).Click();
        }

        [Given(@"entrar com o lacre (.*)")]
        public void GivenEntrarComOLacre(string lLacre)
        {
            sFFConteinerModel.Lacre = lLacre;
            WebDriverWait wait = new WebDriverWait(WDriver, TimeSpan.FromSeconds(30));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='dadosDetalhados']/form/div[2]/div[1]/div/div/input")));

            WDriver.FindElement(By.XPath("//*[@id='dadosDetalhados']/form/div[2]/div[1]/div/div/input")).SendKeys(sFFConteinerModel.Lacre.ToString());
        }

        [Given(@"entrar com Peso Bruto (.*)")]
        public void GivenEntrarComPesoBruto(string lPesoBruto)
        {
            sFFConteinerModel.PesoBruto = lPesoBruto;

            WDriver.FindElement(By.XPath("//*[@id='pesoBruto']")).SendKeys(sFFConteinerModel.PesoBruto.ToString());


            WDriver.FindElement(By.XPath("//*[@id='dadosDetalhados']/form/div[4]/div/a[1]")).Click();
        }

        [Given(@"entrar com cnpjcpf")]
        public void GivenEntrarComCnpjcpf()
        {
            WebDriverWait wait = new WebDriverWait(WDriver, TimeSpan.FromSeconds(10));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.XPath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div/div/section/div/div/div/div/div[3]/div[1]/div[3]/div/p[2]/a/i")));

            WDriver.FindElement(By.XPath("/html/body/div[3]/div[2]/div/div[2]/div/div/div/div/div/section/div/div/div/div/div[3]/div[1]/div[3]/div/p[2]/a/i")).Click();

            WDriver.FindElement(By.XPath("//*[@id='portlet_ConsultaBooking_WAR_ExportacaoCheioVazioPortlet']/div/div/div/div/div[3]/div[9]/div/ul/li[2]/a")).Click();
            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.Id("idCpfCnpjConsignee")));
            WDriver.FindElement(By.Id("idCpfCnpjConsignee")).SendKeys("50567288002011 ");
        }

        [Given(@"entrar com destinatario pais")]
        public void GivenEntrarComDestinatarioPais()
        {
            WebDriverWait wait = new WebDriverWait(WDriver, TimeSpan.FromSeconds(10));
            var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("countryNotify")));

            var selectElement = new SelectElement(WDriver.FindElement(By.Id("countryNotify")));
            selectElement.SelectByValue("CA");
        }

        [Given(@"entrar  com razao social")]
        public void GivenEntrarComRazaoSocial()
        {
            WDriver.FindElement(By.Id("companyNameNotify")).SendKeys("ModalGR Testes Ltda.");
        }

        [Given(@"entrar com a cidade")]
        public void GivenEntrarComACidade()
        {
            WDriver.FindElement(By.Id("cityNotify")).SendKeys("Exterior");
        }

        [Given(@"entrar com tipo NF")]
        public void GivenEntrarComTipoNF()
        {
            var selectElement = new SelectElement(WDriver.FindElement(By.Id("tipoCustomDoc")));
            selectElement.SelectByValue("1420799");
        }

        [Given(@"entrar com numero (.*)")]
        public void GivenEntrarComNumero(string lNumero)
        {
            sFFConteinerModel.Numero = lNumero;
            WDriver.FindElement(By.Id("nbr")).SendKeys(sFFConteinerModel.Numero.ToString());

        }

        [Given(@"entrar com serie")]
        public void GivenEntrarComSerie()
        {
            WDriver.FindElement(By.Id("serie")).SendKeys("01");
        }

        [Given(@"entrar com CFOP")]
        public void GivenEntrarComCFOP()
        {
            var selectElement = new SelectElement(WDriver.FindElement(By.Id("cfop")));
            selectElement.SelectByValue("7.127");
        }

        [Given(@"entrar com a emissao")]
        public void GivenEntrarComAEmissao()
        {
            DateTime dt = DateTime.Today;
            string SDT = dt.ToString("dd/MM/yyyy");
            WDriver.FindElement(By.Id("emissor")).SendKeys(SDT);
        }

        [Given(@"entrar com Volume")]
        public void GivenEntrarComVolume()
        {
            WDriver.FindElement(By.Id("txtVolume")).SendKeys("50");
        }

        [Given(@"entrar com Valor Total")]
        public void GivenEntrarComValorTotal()
        {
            WDriver.FindElement(By.Id("totalAmount")).SendKeys("50000");
        }

        [Given(@"entrar com icms ICMS")]
        public void GivenEntrarComIcmsICMS()
        {
            WDriver.FindElement(By.Id("icms")).SendKeys("100");
        }

        [Given(@"entrar com IPI")]
        public void GivenEntrarComIPI()
        {
            WDriver.FindElement(By.Id("ipi")).SendKeys("100");
        }

        [Given(@"entrar com DANFE (.*)")]
        public void GivenEntrarComDANFE(string lDanfe)
        {
            sFFConteinerModel.Danfe = lDanfe;
            WDriver.FindElement(By.Id("danfe")).SendKeys(sFFConteinerModel.Danfe);
        }

        [When(@"salvar")]
        public void WhenSalvar()
        {
            WDriver.FindElement(By.XPath("//*[@id='collapseOne']/form[3]/div[6]/div/a[1]/i")).Click();

        }

        [Then(@"tela de ok")]
        public void ThenTelaDeOk()
        {
            WDriver.Quit();
        }


    }
}
