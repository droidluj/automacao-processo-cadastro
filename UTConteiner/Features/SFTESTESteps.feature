﻿Feature: SFTESTESteps
	adicionar conteiner e dados complementares


Scenario: Loginse cadastro conteiner
	Given entrar com url
	And entrar com usuario e senha
	And entrar com transportadora <Transportadora>
	And entrar com booking <Booking>
	And Entrar com Unidade <Unidade>
	And editar unidade
	And entrar com o conteiner <Conteiner>
	And confirmar conteiner
	And entrar com o lacre <Lacre>
	And entrar com Peso Bruto <PesoBruto>
	And entrar com cnpjcpf
	And entrar com destinatario pais
	And entrar  com razao social
	And entrar com a cidade
	And entrar com tipo NF
	And entrar com numero <Numero>
	And entrar com serie
	And entrar com CFOP
	And entrar com a emissao
	And entrar com Volume
	And entrar com Valor Total 
	And entrar com icms ICMS
	And entrar com IPI
	And entrar com DANFE <Danfe>
	When salvar
	Then tela de ok

	Examples: 
	| Transportadora            | Booking      | Unidade | Conteiner   | lacre  | PesoBruto | numero  | Danfe                                        |
	| RIOTRANS TRANSPORTES LTDA | BK_AUTOMACAO | 1       | TSTU1112195 | 112233 | 28000     | 1112225 | 33191133247743004450550360000100731122825549 |
	| RIOTRANS TRANSPORTES LTDA | BK_AUTOMACAO | 2       | TSTU1812199 | 112233 | 28000     | 3211058 | 31191138639811000100550010000321521003211058 |
	| RIOTRANS TRANSPORTES LTDA | BK_AUTOMACAO | 3       | TZTU1812193 | 112233 | 8113792   | 1112225 | 31191138639811000100550010000321491008113792 |